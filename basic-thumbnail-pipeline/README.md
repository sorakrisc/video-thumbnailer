# p1(Milestone 1) : The Basic Thumbnail Pipeline

You are building a service that turns a user-supplied video file into a short animated-GIF thumbnail.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prereq.

be in the same directory as Dockerfile and create directory data/frames

```
mkdir -p data/frames
```

## How To Build

```
docker build -t container_name .
```

## How To Run

```
docker run -v $(pwd)/data:/app/data video_thumbnail make_thumbnail Movie.mkv output.gif
```

## Spec

```
ffmpeg -ss $START_TIME -i data/$VID_FILE -vframes 40 -vf fps=3 data/frames/out%02d.jpg  &>/dev/null
```

```-ss``` must come before everything else. Take in start time in seconds.

`-i` inputfile

`-vframes` amount of frames

`-vf fps=XYZ` it will create one thumbnail/frame every XYZ frames of the original video. Where XYZ=1 create 1 frames per sec and 1/60 is one frame every one minute

`%02d.jpg` output image will be formatted using 2 digits

```
convert -resize 320x240!  -delay 20 -loop 0 data/frames/*.jpg data/$OUTPUT_FILE
```

`-resize 320x240!` add ! to force to that dimension if not it will scale nicely

`-delay 20` means the time between each frame is 0.2 seconds





