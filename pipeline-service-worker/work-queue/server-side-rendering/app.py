from flask import Flask, render_template,request, redirect
import requests
import logging
import json


app = Flask(__name__)
SOS_SERVER = 'http://simpleobjectstorage:8080'
QUEUE_WRAPPER_SERVER = 'http://work-queue_queue-wrapper_1:5000'
BASE_URL = "http://localhost:4321"
SOS_SERVER_FOR_HTML = 'http://localhost:8080'

# QUEUE_WRAPPER_SERVER = 'http://localhost:5000'
# BASE_URL = "http://localhost:4322"

SUBMIT_ALL_JOB_API = QUEUE_WRAPPER_SERVER+'/submit-all'
SUBMIT_JOB_API = QUEUE_WRAPPER_SERVER+'/submit'

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

STATUS_OK = requests.codes['ok']

### HELPER
def getAllBucketName():
    resp = requests.get(SOS_SERVER+'/all')
    allBucket = json.loads(resp.content.decode('utf8'))
    # LOG.info("allBucket: %s", allBucket)
    lst =[]
    if allBucket:
        for echBucket in allBucket:
            # LOG.info("ech %s", echBucket)
            bucketname = echBucket.get("bucketName")
            # LOG.info("bucketname %s", bucketname)
            lst.append(bucketname)
    return lst
def getListObjectFromSOS(bucketname):
    resp = requests.get(SOS_SERVER+'/'+bucketname+'?list')
    if resp.status_code == STATUS_OK:
        allObjFiles = json.loads(resp.content.decode('utf8'))
        lstObjects = allObjFiles.get('objects')
        return lstObjects
    else:
        return None

def getAllObjectInBucket(bucketname):
    lstObjects = getListObjectFromSOS(bucketname)
    lstObjName=[]
    if lstObjects:
        for ech in lstObjects:
            objectname = ech.get('name')
            if objectname.endswith(".mp4") or objectname.endswith(".mov") or objectname.endswith(".avi") or objectname.endswith(".mkv"):
                lstObjName.append(objectname)

        return lstObjName
    return None

def getAllGIFInBucket(bucketname):
    lstObjects =getListObjectFromSOS(bucketname)
    lstObjName=[]
    if lstObjects:
        for ech in lstObjects:
            objectname = ech.get('name')
            if objectname.endswith(".gif"):
                lstObjName.append(objectname)
        return lstObjName
    return None

def submit_a_job(body_to_send):
    resp = requests.post(SUBMIT_JOB_API, json=body_to_send)
    return resp.status_code==STATUS_OK
    
def submit_all_job(body_to_send):
    resp = requests.post(SUBMIT_ALL_JOB_API, json=body_to_send)
    return resp.status_code==STATUS_OK

def req_delete_a_gif(bucketname, objectname):
    resp = requests.delete(SOS_SERVER+"/"+bucketname+"/"+objectname+"?delete")
    return resp.status_code==STATUS_OK

def req_delete_all_gif(bucketname):
    lstObjName=getAllGIFInBucket(bucketname)
    status = True
    for echObjName in lstObjName:
        if req_delete_a_gif(bucketname, echObjName):
            continue
        else:
            status = False
    return status
### CALL QUEUE-WRAPPER
@app.route('/submit/<bucketname>/<objectname>' ,methods = ['POST'])
def submit_file(bucketname, objectname):
    body_to_send = {'bucketname': bucketname, 'objectname': objectname, 'targetbucketname': bucketname, 'targetobjectname': objectname+".gif"}
    if submit_a_job(body_to_send):
        return redirect('/show_all_videos/'+bucketname)
    else:
        return render_template('error-page.html', header="not successfull")

@app.route('/submit-all/<bucketname>' ,methods = ['POST'])
def submit_all_file(bucketname):
    body_to_send = {'bucketname': bucketname,'targetbucketname': bucketname}
    if submit_all_job(body_to_send):
        return redirect('/show_all_videos/'+bucketname)
    else:
        return render_template('error-page.html', header="not successfull")
    
@app.route('/delete-all-gif/<bucketname>' ,methods = ['POST'])
def delete_all_gif(bucketname):
    if req_delete_all_gif(bucketname):
        return redirect('/display-room/'+bucketname)
    else:
        return render_template('error-page.html', header="not all is deleted")

@app.route('/delete-a-gif/<bucketname>/<objectname>' ,methods = ['POST'])
def delete_a_gif(bucketname, objectname):
    req_delete_a_gif(bucketname, objectname)
    return redirect('/display-room/'+bucketname)

### HTML RENDERING APIS
@app.route('/show_all_videos/<bucketname>', methods=['POST', 'GET'])
def show_all_videos(bucketname):
    header = "/"+bucketname
    result = getAllObjectInBucket(bucketname)
    if result:
        return render_template('show-all-videos.html', header = header, result = result, bucketname=bucketname, baseurl= BASE_URL)
    else:
        return render_template('error-page.html', header="no video in this bucket")

@app.route('/', methods=['POST', 'GET'])
def student():
    result = getAllBucketName()
    header = "List of Your Buckets"
    return render_template('index.html', header= header, result = result)

@app.route('/display-room/<bucketname>', methods=['POST', 'GET'])
def result(bucketname):
    header = ""
    result=getAllGIFInBucket(bucketname)
    if result:
        return render_template("display-room.html", header=header, bucketname=bucketname, result=result, baseurl= BASE_URL, sosurl= SOS_SERVER_FOR_HTML)
    else:
        return redirect('/show_all_videos/'+bucketname)
