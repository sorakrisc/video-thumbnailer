import os
import json
import requests
import redis
import logging
from flask import Flask, jsonify, request, Response


app = Flask(__name__)
SOS_SERVER = 'http://simpleobjectstorage:8080'
LOG = logging

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:thumbnailing'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def addWorkToQueue(body):
    json_packed = json.dumps(body)
    RedisResource.conn.rpush(
        RedisResource.QUEUE_NAME,
        json_packed)
# API that submits a job to the queue
@app.route('/submit', methods=['POST'])
def post_submit():
    body = request.json
    addWorkToQueue(body)
    return jsonify({'status': 'OK'})

# API that, given a bucket,
# creates multiple jobs for all videos in the bucket
# and submits them to the queue
@app.route('/submit-all', methods=['POST'])
def post_submit_all():
    body = request.json
    bucketname = body.get('bucketname')
    LOG.info('bucketname %s', bucketname)
    targetbucketname = body.get('targetbucketname')
    LOG.info('targetbucketname %s', targetbucketname)
    resp = requests.get(SOS_SERVER+'/'+bucketname+'?list')
    allObjFiles = json.loads(resp.content.decode('utf8'))
    LOG.info('resp.content %s', allObjFiles)
    lstObjects = allObjFiles.get('objects')
    LOG.info('objects %s', lstObjects)
    for ech in lstObjects:
        objectname = ech.get('name')
        if not objectname.endswith(".gif") and not objectname.endswith(".jpg") and not objectname.endswith(".jpeg"):
            body_to_send = {'bucketname': bucketname, 'objectname': objectname, 'targetbucketname': bucketname, 'targetobjectname': objectname+".gif"}
            addWorkToQueue(body_to_send)

        # bodytosend = "{'bucketname': %s, 'objectname': %s, 'targetbucketname': %s, 'targetobjectname': %s}" %( bucketname, name, bucketname, "tarobjame")
        # LOG.info('bodytosend %s', bodytosend)
            # addWorkToQueue("{'bucketname': %s, 'objectname': %s, 'targetbucketname': %s, 'targetobjectname': %s}")
        # LOG.info('ech name %s', ech.get('name'))

    # respJSON = json.dumps(resp.content)
    # print(respJSON.get('created'))
    return jsonify({'status': 'OK'})

# API that lists all GIF images in a user-specified bucket
@app.route('/list-all', methods=['POST'])
def post_list_all():
    body = request.json
    bucketname = body.get('bucketname')
    LOG.info('bucketname %s', bucketname)
    resp = requests.get(SOS_SERVER+'/'+bucketname+'?list')
    allObjFiles = json.loads(resp.content.decode('utf8'))
    lstObjects = allObjFiles.get('objects')
    s = ""
    for echObjects in lstObjects:
        objectname = echObjects.get('name')
        if objectname.endswith(".gif"):
            if len(s) == 0:
                s = objectname
            else:
                s = s+", "+objectname
    return s