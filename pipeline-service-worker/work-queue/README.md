# p1(Milestone 2) : Pipeline as a Service Worker

Implementation and setup of a pipeline for generating thumbnails of videos stored on the Simple Object Storage (SOS).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prereq.

I assume you are in the same directory as the this REAME.md file(video-thumbnailer/pipeline-service-worker/work-queue/README.md)

```
mkdir -p sos/data sos/data-local 

mkdir -p worker/data
```

## How To Build and Run
Specifying how many number of worker needed <number-of-worker>

```
docker-compose up --build --scale worker= <number-of-worker>
```

## Spec.
WORKER

1. that submits a job to the queue
```
http POST http://localhost:5000/submit bucketname=videobucket objectname=az.mp4 targetbucketname=videobucket targetobjectname=az.gif
```

2. API that, given a bucket, creates multiple jobs for all videos in the bucket and submits them to the queue
```
http POST http://localhost:5000/submit-all bucketname=<buckettochangealltogif> targetbucketname=<targetbucketname>
```

3. API that lists all GIF images in a user-specified bucket
```
http POST http://localhost:5000/list-all bucketname=<buckettochangealltogif>
```





