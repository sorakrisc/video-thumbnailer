import requests
SOS_SERVER = 'http://localhost:8080'

def downloadFileFromSOS(bucketname, objectname, target):
    try:
        url = SOS_SERVER+'/'+bucketname+'/'+objectname
        response = requests.get(url=url, stream=True)
        # Throw an error for bad status codes
        response.raise_for_status()
        with open(target, 'wb') as handle:
            for block in response.iter_content(1024):
                handle.write(block)
        handle.close()
        return True
    except Exception as err:
        print(err)
        # LOG.exception('loadFileFromSOS failed')
        return False


print(downloadFileFromSOS('videobucket', 'dogprank.mp4.gif', './loadback.gif'))
